import { useState, useEffect, useCallback } from 'react';
import axios from 'axios';

export function useFetch(url) {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const fetch = useCallback(
    (limit) => {
      setIsLoading(true);

      axios
        .get(url)
        .then((response) => {
          if (limit) {
            setData(response.data.slice(0, limit));
          } else {
            setData(response.data);
          }
          setIsLoading(false);
        })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
    },
    [url]
  );

  const refetch = (params) => {
    setError(null);
    fetch(params.params._limit);
  };

  useEffect(() => {
    fetch();
  }, [fetch]);

  return { data, isLoading, error, refetch };
}
